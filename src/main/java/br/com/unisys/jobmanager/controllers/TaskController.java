package br.com.unisys.jobmanager.controllers;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.unisys.jobmanager.entities.Task;
import br.com.unisys.jobmanager.exceptions.DatabaseException;
import br.com.unisys.jobmanager.exceptions.DateFormatException;
import br.com.unisys.jobmanager.exceptions.EntityNotFoundException;
import br.com.unisys.jobmanager.repositories.TaskRepository;

@RestController
@RequestMapping(value = "tasks")
@Transactional
public class TaskController {
	
	@Value("${spring.jackson.date-format}")
	private String dateFormat;
	
	@Autowired
	private Function<Task, Set<ConstraintViolation<Task>>> validator;
	
	@Autowired
	private TaskRepository taskRepository;
	
	@GetMapping
	public ResponseEntity<List<Task>> getByCreatedAt(
			@RequestParam(name = "$createdAt", required = true) String createdAt) {
		final Date createdAtAsDate;
		try {
			createdAtAsDate = new SimpleDateFormat(dateFormat).parse(createdAt);
		} catch(Exception ex) {
			throw new DateFormatException(dateFormat);
		}
		final List<Task> tasks;
		try {
			tasks = taskRepository.getTaskFromCreatedAt(createdAtAsDate);
		} catch(Exception ex) {
			throw new DatabaseException(ex);
		}
		return ResponseEntity.ok(tasks);
	}
	
	@GetMapping(path = "{id}")
	public ResponseEntity<Task> getById(@PathVariable("id") Integer id) {
		final Optional<Task> optTask;
		try {
			optTask = taskRepository.findById(id);
		} catch(Exception ex) {
			throw new DatabaseException(ex);
		}
		if (optTask.isPresent()) {
			return ResponseEntity.ok(optTask.get());
		} else {
			throw new EntityNotFoundException();	
		}
	}
	
	private void saveOrUpdate(Task task) {
		final Set<ConstraintViolation<Task>> violations = validator.apply(task);
		if ((violations != null) && (!violations.isEmpty())) {
			throw new ConstraintViolationException(violations);
		} else {
			if (task.getCreatedAt() == null) {
				task.setCreatedAt(Date.from(Instant.now()));
			} else {
				final Calendar calendarToRemoveHours = Calendar.getInstance();
				calendarToRemoveHours.setTime(task.getCreatedAt());
				calendarToRemoveHours.set(Calendar.HOUR_OF_DAY, 0);
				calendarToRemoveHours.set(Calendar.MINUTE, 0);
				calendarToRemoveHours.set(Calendar.SECOND, 0);
				calendarToRemoveHours.set(Calendar.MILLISECOND, 0);
				task.setCreatedAt(calendarToRemoveHours.getTime());
			}
			try {
				taskRepository.save(task);
			} catch(Exception ex) {
				throw new DatabaseException(ex);
			}
			
		}		
	}
	
	@PostMapping
	public void post(@RequestBody Task task) {
		saveOrUpdate(task);
	}	
	
	@PutMapping(path = "{id}")
	public void put(@PathVariable("id") Integer id, @RequestBody Task task) {
		saveOrUpdate(task);
	}
	
	@DeleteMapping(path = "{id}")
	public void delete(@PathVariable("id") Integer id) {
		final Optional<Task> optTask;
		try {
			optTask = taskRepository.findById(id);
		} catch (Exception ex) {
			throw new DatabaseException(ex);
		}
		if (optTask.isPresent()) {
			try {
				taskRepository.delete(optTask.get());	
			} catch(Exception ex) {
				throw new DatabaseException(ex);
			}
				
		} else {
			throw new EntityNotFoundException();	
		}
	}	
	
}
