package br.com.unisys.jobmanager.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.unisys.jobmanager.entities.Job;
import br.com.unisys.jobmanager.exceptions.DatabaseException;
import br.com.unisys.jobmanager.exceptions.EntityNotFoundException;
import br.com.unisys.jobmanager.exceptions.JobDependenceException;
import br.com.unisys.jobmanager.repositories.JobRepository;

@RestController
@RequestMapping(value = "jobs")
@Transactional
public class JobController {
	
	@Autowired
	private Function<Job, Set<ConstraintViolation<Job>>> validator;	

	@Autowired
	private JobRepository jobRepository;
	
	@Autowired
	private Predicate<Job> jobDependenceValidator;

	@GetMapping
	public ResponseEntity<List<Job>> get(@RequestParam(value = "sortByWeight", required = false) Boolean sortByWeight) {
		final List<Job> jobs;
		try {
			jobs = jobRepository.findAll();	
		} catch(Exception ex) {
			throw new DatabaseException(ex);
		}
		if ((sortByWeight != null) && (sortByWeight.booleanValue())) {
			Collections.sort(jobs, (job1, job2) -> job1.getSumTaskWeight().compareTo(job2.getSumTaskWeight()));
		}
		return ResponseEntity.ok(jobs);
	}
	
	@GetMapping(path = "{id}")
	public ResponseEntity<Job> getById(@PathVariable("id") Integer id) {
		final Optional<Job> optJob;
		try {
			optJob = jobRepository.findById(id);
		} catch(Exception ex) {
			throw new DatabaseException(ex);
		}
		if (optJob.isPresent()) {
			return ResponseEntity.ok(optJob.get());
		} else {
			throw new EntityNotFoundException();	
		}
	}
	
	private void saveOrUpdate(Job job) {
		final Set<ConstraintViolation<Job>> violations = validator.apply(job);
		if ((violations != null) && (!violations.isEmpty())) {
			throw new ConstraintViolationException(violations);
		} else {
			if (!jobDependenceValidator.test(job)) {
				throw new JobDependenceException(job.getName());
			}
			try {
				jobRepository.save(job);
			} catch(Exception ex) {
				throw new DatabaseException(ex);
			}
			
		}		
	}
	
	@PostMapping
	public void post(@RequestBody Job job) {
		saveOrUpdate(job);
	}	
	
	@PutMapping(path = "{id}")
	public void put(@PathVariable("id") Integer id, @RequestBody Job job) {
		saveOrUpdate(job);
	}
	
	@DeleteMapping(path = "{id}")
	public void delete(@PathVariable("id") Integer id) {
		final Optional<Job> optJob;
		try {
			optJob = jobRepository.findById(id);
		} catch (Exception ex) {
			throw new DatabaseException(ex);
		}
		if (optJob.isPresent()) {
			try {
				jobRepository.delete(optJob.get());	
			} catch(Exception ex) {
				throw new DatabaseException(ex);
			}
				
		} else {
			throw new EntityNotFoundException();	
		}
	}	

}
















