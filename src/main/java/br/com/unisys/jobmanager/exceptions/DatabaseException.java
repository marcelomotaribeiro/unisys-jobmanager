package br.com.unisys.jobmanager.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED)
public class DatabaseException extends RuntimeException {

	private static final long serialVersionUID = 6373204251860794838L;

	public DatabaseException(Exception ex) {
		super(ex);
	}

}
