package br.com.unisys.jobmanager.exceptions;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ConstraintViolationException extends RuntimeException {

	private static final long serialVersionUID = 757411403663709290L;

	public ConstraintViolationException(Set<ConstraintViolation<?>> violations) {
		super(violations.stream().map(ConstraintViolation::getMessage)
				.collect(Collectors.joining(System.lineSeparator())));
	}

}
