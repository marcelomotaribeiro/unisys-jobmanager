package br.com.unisys.jobmanager.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DateFormatException extends RuntimeException {

	private static final long serialVersionUID = -3949129884760500664L;

	public DateFormatException(String dateFormat) {
		super("Invalid date format. Use: ".concat(dateFormat == null ? "" : dateFormat).concat("."));
	}
	
}
