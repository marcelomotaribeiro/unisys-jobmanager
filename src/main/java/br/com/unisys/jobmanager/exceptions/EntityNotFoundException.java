package br.com.unisys.jobmanager.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -4961996063964442192L;

	public EntityNotFoundException() {
		super("Entity not found!");
	}
	
}
