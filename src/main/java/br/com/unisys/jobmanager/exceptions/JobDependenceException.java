package br.com.unisys.jobmanager.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class JobDependenceException extends RuntimeException {

	private static final long serialVersionUID = 7387539379776158199L;

	public JobDependenceException(String jobName) {
		super("Job can't be self dependence: ".concat(jobName == null ? "(no name)" : jobName));
	}
	
}
