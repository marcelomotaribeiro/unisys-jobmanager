package br.com.unisys.jobmanager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.unisys.jobmanager.entities.Job;

@Repository
@Transactional
public interface JobRepository extends JpaRepository<Job, Integer> {
	
	List<Job> findByName(@Param("name") String name);

}
