package br.com.unisys.jobmanager.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.unisys.jobmanager.entities.Task;

@Repository
@Transactional
public interface TaskRepository extends JpaRepository<Task, Integer> {
	
	@Query(value = "SELECT t FROM Task t WHERE t.createdAt >= :createdAt")
	List<Task> getTaskFromCreatedAt(@Param("createdAt") Date createdAt);
	
}
