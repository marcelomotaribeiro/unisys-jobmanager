package br.com.unisys.jobmanager.business;

import java.util.function.Supplier;

import javax.annotation.PostConstruct;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.stereotype.Component;

@Component
public class GenericValidator implements Supplier<Validator> {

	private Validator validator;

	@PostConstruct
	private void init() {
		final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Override
	public Validator get() {
		return validator;
	}


}
