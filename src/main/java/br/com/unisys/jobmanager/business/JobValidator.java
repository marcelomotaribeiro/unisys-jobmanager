package br.com.unisys.jobmanager.business;

import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.unisys.jobmanager.entities.Job;

@Component
@Transactional
public class JobValidator implements Function<Job, Set<ConstraintViolation<Job>>> {
	
	@Autowired
	private Supplier<Validator> validator;
	
	@Override
	public Set<ConstraintViolation<Job>> apply(Job job) {
		return validator.get().validate(job);
	}


}
