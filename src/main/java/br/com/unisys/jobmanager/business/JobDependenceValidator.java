package br.com.unisys.jobmanager.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.unisys.jobmanager.entities.Job;
import br.com.unisys.jobmanager.exceptions.DatabaseException;
import br.com.unisys.jobmanager.repositories.JobRepository;

@Component
public class JobDependenceValidator implements Predicate<Job> {

	@Autowired
	private JobRepository jobRepository;

	private Job getParent(Job job) {
		if ((job.getJobParent() != null) && (job.getJobParent().getId() != null)) {
			final Optional<Job> optJobParent;
			try {
				optJobParent = jobRepository.findById(job.getJobParent().getId());
			} catch (Exception ex) {
				throw new DatabaseException(ex);
			}
			if (optJobParent.isPresent()) {
				return optJobParent.get();
			}
		}
		return null;
	}

	public static Boolean checkJobEqualityByName(Job jobA, Job jobB) {
		return (jobA.getName() == null && jobB.getName() == null)
				|| ((jobA.getName() != null) && (jobB.getName() != null) && jobA.getName().equals(jobB.getName()));
	}

	@Override
	public boolean test(Job job) {
		if (job.getJobParent() != null) {
			final List<Job> parents = new ArrayList<>();
			parents.add(job);
			Job parent = getParent(job);
			while (parent != null) {
				final Job parentToCheck = parent;
				final Optional<Job> invalidJob = parents.stream().filter(x -> checkJobEqualityByName(x, parentToCheck))
						.findFirst();
				if (invalidJob.isPresent()) {
					return false;
				} else {
					parents.add(parent);
				}
				if (parent.getJobParent() != null) {
					parent = getParent(parent.getJobParent());	
				} else {
					parent = null;
				}
			}
		}
		return true;
	}

}
