package br.com.unisys.jobmanager.business;

import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.unisys.jobmanager.entities.Task;

@Component
@Transactional
public class TaskValidator implements Function<Task, Set<ConstraintViolation<Task>>> {

	@Autowired
	private Supplier<Validator> validator;
	
	@Override
	public Set<ConstraintViolation<Task>> apply(Task task) {
		return validator.get().validate(task);
	}

}
