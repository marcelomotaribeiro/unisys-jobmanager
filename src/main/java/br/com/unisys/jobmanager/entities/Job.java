package br.com.unisys.jobmanager.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Job {

	@Id
	@Column(nullable = false)
	@NotNull
	private Integer id;
	
	@Column(nullable = false)
	@NotNull
	private String name;
	
	@Column(nullable = false)
	@NotNull
	private Boolean active = Boolean.FALSE;
	
	@ManyToOne
	private Job jobParent;
	
	@OneToMany
	private List<Task> items = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Job getJobParent() {
		return jobParent;
	}

	public void setJobParent(Job jobParent) {
		this.jobParent = jobParent;
	}

	public List<Task> getItems() {
		return items;
	}

	public void setItems(List<Task> items) {
		this.items = items;
	}
	
	@Transient
	@JsonIgnore
	public Double getSumTaskWeight() {
		if ((items != null) && (!items.isEmpty())) {
			return items.stream()
					.filter(task -> task.getWeight() != null)
					.mapToDouble(Task::getWeight)
					.sum();
		}
		return 0D;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
