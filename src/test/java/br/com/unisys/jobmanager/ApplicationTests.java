package br.com.unisys.jobmanager;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.unisys.jobmanager.business.JobDependenceValidator;
import br.com.unisys.jobmanager.entities.Job;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	private final Job createJobForTest(String jobName) {
		final Job job = new Job();
		job.setName(jobName);
		return job;
	}

	@Test
	public void checkJobEqualityByNameTestOk() {
		final Job jobA = createJobForTest("A");
		final Job jobB = createJobForTest("B");
		Assert.assertFalse(JobDependenceValidator.checkJobEqualityByName(jobA, jobB));

	}

	@Test
	public void checkJobEqualityByNameTestFail() {
		final Job jobA = createJobForTest("A");
		final Job jobB = createJobForTest("A");
		Assert.assertTrue(JobDependenceValidator.checkJobEqualityByName(jobA, jobB));
	}

}
